# Colossal Raw Asset Package Tool

## Description

Visual Studio 2017 c# project
A tool designed to Unpack and Re-pack the *.crp files. Is a work in progress, the idea is to convert the crp file in a series of editable files or known formats
[img]https://preview.ibb.co/iuQwdA/previewcrptool.jpg[/img]

Crp.bt : Template file for 010Editor Hex editor program. It's used to investigate the file structure.
[img]https://preview.ibb.co/eY6mdA/preview010editorcrptool.png[/img]

## Fbx conversion of static mesh
work in progress... got to https://bitbucket.org/johnwhile/fbxwrapper will be useful also for other projects.
A make a backup in FbxSdkWrapper_compiled.zip but it will not be updated often !

Thanks to Colossal Order for not have obfuscated the game's dll
