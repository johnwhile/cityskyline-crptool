﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrpTool
{
    /// <summary>
    /// <seealso cref="public class AssetType" in ColossalFramework/>
    /// <seealso cref="UserAssetType" int Assembly-CSharp.dll/>
    /// </summary>
    public enum AssetType : int
    {
        Unknow = 0,
        Object = 1,
        Material = 2,
        Texture = 3,
        StaticMesh = 4,
        Text = 50,
        Assembly = 51,
        Data = 52,
        Package = 53,
        Locale = 80,
        LocaleOverride = 81,

        User = 100, // or MapMetaData
        SaveGameMetaData = User + 1,
        SystemMapMetaData = User + 2,
        CustomAssetMetaData = User + 3,
        ColorCorrection = User + 4,
        DistrictStyleMetaData = User + 5,
        MapThemeMetaData = User + 5,
        MapThemeMapMetaData = User + 7,
        ScenarioMetaData = User + 8
    }

    [Flags]
    public enum Availability
    {
        None = 0,
        Game = 1,
        MapEditor = 2,
        GameAndMap = 3,
        AssetEditor = 4,
        GameAndAsset = 5,
        MapAndAsset = 6,
        ThemeEditor = 8,
        ScenarioEditor = 16,
        Editors = 254,
        All = 255
    }

    /// <summary>
    /// ItemClass+Service
    /// </summary>
    public enum Service
    {
        None = 0,
        Residential = 1,
        Commercial = 2,
        Industrial = 3,
        Natural = 4,
        Unused2 = 5,
        Citizen = 6,
        Tourism = 7,
        Office = 8,
        Road = 9,
        Electricity = 10,
        Water = 11,
        Beautification = 12,
        Garbage = 13,
        HealthCare = 14,
        PoliceDepartment = 15,
        Education = 16,
        Monument = 17,
        FireDepartment = 18,
        PublicTransport = 19,
        Disaster = 20
    }

    /// <summary>
    /// ItemClass+SubService
    /// </summary>
    public enum SubService
    {
        None = 0,
        ResidentialLow = 1,
        ResidentialHigh = 2,
        CommercialLow = 3,
        CommercialHigh = 4,
        IndustrialGeneric = 5,
        IndustrialForestry = 6,
        IndustrialFarming = 7,
        IndustrialOil = 8,
        IndustrialOre = 9,
        PublicTransportBus = 10,
        PublicTransportMetro = 11,
        PublicTransportTrain = 12,
        PublicTransportShip = 13,
        PublicTransportPlane = 14,
        PublicTransportTaxi = 15,
        PublicTransportTram = 16,
        BeautificationParks = 17,
        CommercialLeisure = 18,
        CommercialTourist = 19,
        PublicTransportMonorail = 20,
        PublicTransportCableCar = 21,
        OfficeGeneric = 22,
        OfficeHightech = 23,
        CommercialEco = 24,
        ResidentialLowEco = 25,
        ResidentialHighEco = 26,
        PublicTransportTours = 27,
        Reserved = 28
    }

    /// <summary>
    /// ItemClass+Level
    /// </summary>
    public enum Level
    {
        None = -1,
        Level1 = 0,
        Level2 = 1,
        Level3 = 2,
        Level4 = 3,
        Level5 = 4
    }

    [Flags]
    public enum Layer
    {
        None = 0,
        Default = 1,
        WaterPipes = 2,
        PublicTransport = 4,
        MetroTunnels = 8,
        AirplanePaths = 16,
        ShipPaths = 32,
        Markers = 64,
        WaterStructures = 128,
        BlimpPaths = 256
    }

    public enum Zone
    {
        Unzoned = 0,
        Distant = 1,
        ResidentialLow = 2,
        ResidentialHigh = 3,
        CommercialLow = 4,
        CommercialHigh = 5,
        Industrial = 6,
        Office = 7,
        None = 15
    }

    public enum Placement
    {
        Manual = 0,
        Automatic = 1,
        Procedural = 2
    }

    public enum CollisionType
    {
        Undefined = 0,
        Underground = 1,
        Terrain = 2,
        Elevated = 4,
        Zoned = 8
    }

    /// <summary>
    /// VehicleInfo+VehicleType
    /// </summary>
    [Flags]
    public enum VehicleType
    {
        None = 0,
        Car = 1,
        Metro = 2,
        Train = 4,
        Ship = 8,
        Plane = 16,
        Bicycle = 32,
        Tram = 64,
        Helicopter = 128,
        Meteor = 256,
        Vortex = 512,
        Ferry = 1024,
        Monorail = 2048,
        CableCar = 4096,
        Blimp = 8192,
        Balloon = 16384,
        Rocket = 32768,
        All = 65535
    }
}
