﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace CrpTool
{
    /// <summary>
    /// Match with MD5 Checksum code
    /// </summary>
    public class ObjectReference
    {
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        //public char[] c;

        public string checksum;
        public PackageAsset reference;

        public ObjectReference(string checksum)
        {
            //c = checksum.ToCharArray(0, 32);
            this.checksum = checksum;
        }

        public ObjectReference(PackageAsset asset)
        {
            checksum = asset.Checksum;
            //c = asset.Checksum.ToCharArray(0, 32);
        }

        public override string ToString()
        {
            return checksum;
            //return new string(c);
        }
    }

    /// <summary>
    /// I prefer my custom serialization
    /// </summary>
    public interface IMySerialized
    {
        /// <summary>
        /// Read the binary data and return true if is possible continue
        /// </summary>
        bool Deserialize(BinaryReader reader);
        /// <summary>
        /// Write the binary data and return false if something wrong
        /// </summary>
        bool Serialize(BinaryWriter writer);
    }

    public interface IMyExportable
    {
        Result Export(string fullfilename);
    }


    public delegate object DeserializerDelegate(BinaryReader reader);
    public delegate void SerializerDelegate<T>(BinaryWriter writer, T value);

    public enum Result
    {
        OK,
        IsNull,
        Error,
        NoCreateFilePermission,
        NoReadFilePermission,
        NoWriteFilePermission,
        NotImplemented
    }

    public enum MyDataType
    {
        Invalid = -1,
        Unknown = 0,
        Byte = 1,
        Short = 2,
        Int = 3,
        Long = 4,
        Single = 5,
        Double = 6,
        Vector2 = 10,
        Vector3 = 11,
        Quaternion = 12,
        Transform = 13,

        ByteArray = 1 | 256,
        ShortArray = 2 | 256,
        IntArray = 3 | 256,
        LongArray = 4 | 256,
        SingleArray = 5 | 256,
        DoubleArray = 6 | 256,
        Vector2Array = 10 | 256,
        Vector3Array = 11 | 256
    }



    public static class Helper
    {
        // found in ColossalManaged.dll
        public const string ColossalFrameworkMD5 = "dba5ac35a59f1684220278cd99d569b4";

        public const int MAXDESERIALIZESIZE = ushort.MaxValue;


        public static Dictionary<string, DeserializerDelegate> Parser;


        static Helper()
        {
            Parser = new Dictionary<string, DeserializerDelegate>();
            
            Parser.Add("System.Byte", ByteDeserializer);
            Parser.Add("System.Int16", ShortDeserializer);
            Parser.Add("System.Int32", IntDeserializer);
            Parser.Add("System.Single", FloatDeserializer);
            Parser.Add("System.String", ReadString);
            Parser.Add("System.DateTime", DataTimeDeserializer);
            Parser.Add("System.Byte[]", ByteArrayDeserializer);
            Parser.Add("System.Int16[]", ShortArrayDeserializer);
            Parser.Add("System.Int32[]", IntArrayDeserializer);
            Parser.Add("System.Single[]", FloatArrayDeserializer);
            Parser.Add("System.String[]", StringArrayDeserializer);

            Parser.Add("UnityEngine.Vector2", Vector2Deserializer);
            Parser.Add("UnityEngine.Vector3", Vector3Deserializer);
            Parser.Add("UnityEngine.Vector2[]", Vector2ArrayDeserializer);
            Parser.Add("UnityEngine.Vector3[]", Vector3ArrayDeserializer);
            Parser.Add("UnityEngine.Quaternion", QuaternionDeserializer);
            Parser.Add("UnityEngine.Transform", TransformDeserializer);

            Parser.Add("ColossalFramework.Packaging.Package.Asset", AssetRefDeserializer);
            Parser.Add("CustomAssetMetaData.Type", IntDeserializer);
            Parser.Add("ModInfo[]", ModInfoArrayDeserializer);
            Parser.Add("ItemClass.Level", IntDeserializer);
            Parser.Add("ItemClass.Service", IntDeserializer);
            Parser.Add("ItemClass.SubService", IntDeserializer);
            Parser.Add("VehicleInfo.VehicleType", IntDeserializer);
            Parser.Add("SteamHelper.DLC_BitMask", IntDeserializer);
        }

        static object Vector2Deserializer(BinaryReader reader)
        {
            return Vector2.Deserializer(reader);
        }
        static object Vector3Deserializer(BinaryReader reader)
        {
            return Vector3.Deserializer(reader);
        }

        static object ModInfoDeserializer(BinaryReader reader)
        {
            ModInfo info = new ModInfo();
            info.modName = reader.ReadString();
            info.modWorkshopID = reader.ReadUInt64();
            return info;
        }

        static object ModInfoArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            Debug.Assert(count < MAXDESERIALIZESIZE, "Array too big, stop to safety");
            ModInfo[] data = new ModInfo[count];
            for (int i = 0; i < count; i++) data[i] = (ModInfo)ModInfoDeserializer(reader);
            return data;
        }

        static object AssetRefDeserializer(BinaryReader reader)
        {
            string reference = reader.ReadString();
            return new ObjectReference(reference);
        }

        static object DataTimeDeserializer(BinaryReader reader)
        {
            string date = reader.ReadString();
            return DateTime.Parse(date);
        }
        static object ByteDeserializer(BinaryReader reader)
        {
            return reader.ReadByte();
        }
        static object ShortDeserializer(BinaryReader reader)
        {
            return reader.ReadInt16();
        }
        static object IntDeserializer(BinaryReader reader)
        {
            return reader.ReadInt32();
        }
        static object FloatDeserializer(BinaryReader reader)
        {
            return reader.ReadSingle();
        }
        static object ByteArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            return reader.ReadBytes(count);
        }

        static object StringArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            Debug.Assert(count < MAXDESERIALIZESIZE, "Array too big, stop to safety");
            string[] data = new string[count];
            for (int i = 0; i < count; i++) data[i] = reader.ReadString();
            return data;
        }

        static object ShortArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            Debug.Assert(count < MAXDESERIALIZESIZE, "Array too big, stop to safety");
            Int16[] data = new Int16[count];
            for (int i = 0; i < count; i++) data[i] = reader.ReadInt16();
            return data;
        }
        static object IntArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            Debug.Assert(count < MAXDESERIALIZESIZE, "Array too big, stop to safety");
            Int32[] data = new Int32[count];
            for (int i = 0; i < count; i++) data[i] = reader.ReadInt32();
            return data;
        }
        static object FloatArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            Debug.Assert(count < MAXDESERIALIZESIZE, "Array too big, stop to safety");
            Single[] data = new Single[count];
            for (int i = 0; i < count; i++) data[i] = reader.ReadSingle();
            return data;
        }

        static object Vector2ArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            Debug.Assert(count < MAXDESERIALIZESIZE, "Array too big, stop to safety");
            Vector2[] data = new Vector2[count];
            for (int i = 0; i < count; i++) data[i] = (Vector2)Vector2.Deserializer(reader);
            return data;
        }


        static object Vector3ArrayDeserializer(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            Debug.Assert(count < MAXDESERIALIZESIZE, "Array too big, stop to safety");
            Vector3[] data = new Vector3[count];
            for (int i = 0; i < count; i++) data[i] = (Vector3)Vector3.Deserializer(reader);
            return data;
        }

        static object QuaternionDeserializer(BinaryReader reader)
        {
            return Vector4.Deserializer(reader);
        }

        static object TransformDeserializer(BinaryReader reader)
        {
            return new TranformMatrix(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(),
                                      reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(),
                                      reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }



        public static string ReadString(BinaryReader reader)
        {
            return reader.ReadString();
            //int length = reader.ReadByte();
            //if (length <= 0) return null;
            //return new string(reader.ReadChars(length));
        }

        public static string CalculateChecksum(Stream memorystream)
        {
            using (MD5 md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(memorystream)).Replace("-", "").ToLower();
            }
        }

        public static AssetType RemapAssetType(AssetType type)
        {
            switch ((int)type)
            {
                case 5: return AssetType.Text;
                case 6: return AssetType.Assembly;
                case 7: return AssetType.Data;
                case 8: return AssetType.Package;
                default: return type;
            }
        }

        public static Type TryResolveType(string type)
        {
            return Type.GetType(type);
        }
    }
}
