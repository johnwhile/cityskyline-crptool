﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

namespace CrpTool
{
    /// <summary>
    /// reverse engineering of Colossal Raw Asset Package
    /// </summary>
    public class PackageFile : IMySerialized
    {
        public string foldertosave;

        internal PackageHeader header;

        public Dictionary<string, PackageAsset> AssetList;

        /// <summary>
        /// fill path name
        /// </summary>
        public string fullpath { get; private set; }

        public AssetType AssetType
        {
            get { return AssetType.Package; }
        }

        public PackageFile()
        {
        }

        /// <summary>
        /// Open the filename (fullpath) and deserialize it
        /// </summary>
        /// <param name="filename"></param>
        public Result OpenFile(string filename)
        {
            fullpath = filename;

            using (FileStream file = new FileStream(filename, FileMode.Open))
            {
                Debug.Assert(file.CanRead, "Can't read " + filename);

                using (BinaryReader reader = new BinaryReader(file))
                {
                    if (!reader.BaseStream.CanRead) return Result.NoReadFilePermission;

                    if ( ! Deserialize(reader))
                    {
                        reader.Dispose();
                        return Result.Error;
                    }
                }

                file.Close();
            }
            return Result.OK;
        }

        /// <summary>
        /// Save the filename (fullpath) and serialize it
        /// </summary>
        /// <param name="filename"></param>
        public Result SaveFile(string filename)
        {
            throw new NotImplementedException();

            using (FileStream file = new FileStream(filename, FileMode.CreateNew))
            {
                Debug.Assert(file.CanWrite, "Can't write " + filename);

                using (BinaryWriter writer = new BinaryWriter(file))
                {
                    if (!writer.BaseStream.CanWrite) return Result.NoWriteFilePermission;

                    if (!Serialize(writer))
                    {
                        writer.Dispose();
                        return Result.Error;
                    }
                }

                file.Close();
            }
            return Result.OK;
        }

        /// <summary>
        /// Create the editables output files of package
        /// </summary>
        public Result ExportToFiles()
        {
            string m_filename = Path.GetFileNameWithoutExtension(fullpath);
            string m_directory = Path.GetDirectoryName(fullpath);
            foldertosave = Path.Combine(m_directory, m_filename);

            Debug.WriteLine("Creating Asset files...");

            Directory.CreateDirectory(foldertosave);
                if (!Directory.Exists(foldertosave)) return Result.NoCreateFilePermission;

            foreach (PackageAsset asset in AssetList.Values)
            {
                Debug.WriteLine(string.Format("Exporting asset{0} {1}", asset.index, asset.AssetName));
                Result res = asset.ExportData(foldertosave);
                Debug.WriteLineIf(res != Result.OK, "#ERR! : can't export asset's data");
            }

            return Result.OK;
        }

        /// <summary>
        /// Read the stream and build the classes
        /// </summary>
        public bool Deserialize(BinaryReader reader)
        {
            Debug.WriteLine("Reading file header...");
            header = new PackageHeader();

            if (!header.Deserialize(reader)) return false;

            AssetList = new Dictionary<string, PackageAsset>(header.AssetCount);

            Debug.WriteLine("Assets Count " + header.AssetCount);

            for (int i = 0; i < header.AssetCount; i++)
            {
                Debug.WriteLine("> asset" + i);

                // fullname is composed by asset's name + checksum string
                string fullname = Helper.ReadString(reader);
                string checksum = Helper.ReadString(reader);
                AssetType type = (AssetType)reader.ReadInt32();
                long offset = reader.ReadInt64();
                long size = reader.ReadInt64();
                if (header.PackageVersion < 2) type = Helper.RemapAssetType(type);

                PackageAsset asset = new PackageAsset(this, fullname, checksum, type, offset, size, i);

                AssetList.Add(checksum, asset);
            }

            Debug.WriteLine("Reading assets...");

            foreach (PackageAsset asset in AssetList.Values)
            {
                if (!asset.Deserialize(reader)) return false;
            }
            return true;
        }
        

        public bool Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
