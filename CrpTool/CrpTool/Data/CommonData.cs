﻿using System;
using System.IO;


namespace CrpTool
{
    public class Triangle
    {
        public int[] index = new int[3];

        public Triangle(int i, int j, int k)
        {
            index[0] = i;
            index[1] = j;
            index[2] = k;
        }

        public static Triangle Deserializer(BinaryReader reader)
        {
            return new Triangle(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
        }
    }

    public struct Vector3
    {
        public float x, y, z;
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3 Deserializer(BinaryReader reader)
        {
            return new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }
    }

    public struct Vector2
    {
        public float x, y;
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2 Deserializer(BinaryReader reader)
        {
            return new Vector2(reader.ReadSingle(), reader.ReadSingle());
        }
    }

    public struct Vector4
    {
        public float x, y, z, w;
        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public static Vector4 Deserializer(BinaryReader reader)
        {
            return new Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
        }
    }

    public struct Matrix4x4
    {
        public Vector4 row0, row1, row2, row3;

        public Matrix4x4(Vector4 row0, Vector4 row1, Vector4 row2, Vector4 row3)
        {
            this.row0 = row0;
            this.row1 = row1;
            this.row2 = row2;
            this.row3 = row3;
        }

        public static Matrix4x4 Deserializer(BinaryReader reader)
        {
            Matrix4x4 mat = new Matrix4x4();
            mat.row0 = Vector4.Deserializer(reader);
            mat.row1 = Vector4.Deserializer(reader);
            mat.row2 = Vector4.Deserializer(reader);
            mat.row3 = Vector4.Deserializer(reader);
            return mat;
        }
    }

    public struct BoneWeight
    {
        public int index0, index1, index2, index3;
        public float weight0, weight1, weight2, weight3;

        public BoneWeight(int index0, float weight0, int index1, float weight1, int index2, float weight2, int index3, float weight3)
        {
            this.index0 = index0;
            this.index1 = index1;
            this.index2 = index2;
            this.index3 = index3;

            this.weight0 = weight0;
            this.weight1 = weight1;
            this.weight2 = weight2;
            this.weight3 = weight3;
        }

        public static BoneWeight Deserializer(BinaryReader reader)
        {
            BoneWeight bw = new BoneWeight();
            bw.index0 = reader.ReadInt32();
            bw.weight0 = reader.ReadSingle();
            bw.index1 = reader.ReadInt32();
            bw.weight1 = reader.ReadSingle();
            bw.index2 = reader.ReadInt32();
            bw.weight2 = reader.ReadSingle();
            bw.index3 = reader.ReadInt32();
            bw.weight3 = reader.ReadSingle();
            return bw;
        }
    }

    public struct TranformMatrix
    {
        public Vector3 position;
        public Vector4 rotation;
        public Vector3 scale;

        public TranformMatrix(float x, float y, float z, float qx, float qy, float qz, float qw, float sx, float sy, float sz)
        {
            position = new Vector3(x, y, z);
            rotation = new Vector4(qx, qy, qz, qw);
            scale = new Vector3(sx, sy, sz);
        }
        public TranformMatrix Deserializer(BinaryReader reader)
        {
            TranformMatrix mat = new TranformMatrix();
            mat.position = Vector3.Deserializer(reader);
            mat.rotation = Vector4.Deserializer(reader);
            mat.scale = Vector3.Deserializer(reader);
            return mat;
        }

    }

}
