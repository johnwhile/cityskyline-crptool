﻿namespace CrpTool
{
    partial class MyAppForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUnpack = new System.Windows.Forms.Button();
            this.btnPack = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opencrpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savecrpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileTreeView = new System.Windows.Forms.TreeView();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnUnpack
            // 
            this.btnUnpack.Location = new System.Drawing.Point(187, 27);
            this.btnUnpack.Name = "btnUnpack";
            this.btnUnpack.Size = new System.Drawing.Size(208, 53);
            this.btnUnpack.TabIndex = 0;
            this.btnUnpack.Text = "Unpack *.crp";
            this.btnUnpack.UseVisualStyleBackColor = true;
            this.btnUnpack.Click += new System.EventHandler(this.btnUnpack_Click);
            // 
            // btnPack
            // 
            this.btnPack.Location = new System.Drawing.Point(401, 27);
            this.btnPack.Name = "btnPack";
            this.btnPack.Size = new System.Drawing.Size(208, 53);
            this.btnPack.TabIndex = 1;
            this.btnPack.Text = "Pack *.crp";
            this.btnPack.UseVisualStyleBackColor = true;
            this.btnPack.Click += new System.EventHandler(this.btnPack_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "crp";
            this.openFileDialog.Filter = "Colossal Raw Asset Package (*.crp)|*.crp";
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Unpack Crp file";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.fileMenuItem,
            this.editMenuItem,
            this.infoMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(828, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(12, 20);
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opencrpMenuItem,
            this.savecrpMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileMenuItem.Text = "File";
            // 
            // opencrpMenuItem
            // 
            this.opencrpMenuItem.Name = "opencrpMenuItem";
            this.opencrpMenuItem.Size = new System.Drawing.Size(152, 22);
            this.opencrpMenuItem.Text = "Open *.crp";
            // 
            // savecrpMenuItem
            // 
            this.savecrpMenuItem.Name = "savecrpMenuItem";
            this.savecrpMenuItem.Size = new System.Drawing.Size(152, 22);
            this.savecrpMenuItem.Text = "Save *crp";
            // 
            // editMenuItem
            // 
            this.editMenuItem.Name = "editMenuItem";
            this.editMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editMenuItem.Text = "Edit";
            // 
            // infoMenuItem
            // 
            this.infoMenuItem.Name = "infoMenuItem";
            this.infoMenuItem.Size = new System.Drawing.Size(24, 20);
            this.infoMenuItem.Text = "?";
            // 
            // FileTreeView
            // 
            this.FileTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.FileTreeView.Location = new System.Drawing.Point(0, 24);
            this.FileTreeView.Name = "FileTreeView";
            this.FileTreeView.Size = new System.Drawing.Size(124, 395);
            this.FileTreeView.TabIndex = 3;
            // 
            // MyAppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(828, 419);
            this.Controls.Add(this.FileTreeView);
            this.Controls.Add(this.btnPack);
            this.Controls.Add(this.btnUnpack);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MyAppForm";
            this.Text = "Crp tool";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUnpack;
        private System.Windows.Forms.Button btnPack;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opencrpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savecrpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoMenuItem;
        private System.Windows.Forms.TreeView FileTreeView;
    }
}

