﻿using System;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace CrpTool
{
    /// <summary>
    /// Assembly resolver
    /// </summary>
    public class ParsedObject : IMySerialized
    {
        /// <summary>
        /// a nullable object, if true doesn't exist other member
        /// </summary>
        public bool isNull { get; set; }
        /// <summary>
        /// can be the name of a class member
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// The resolved type, null if can't find
        /// </summary>
        public Type ObjType { get; private set; }
        /// <summary>
        /// The string used to describe object
        /// </summary>
        public ParsedString Parsed;


        public bool Deserialize(BinaryReader reader)
        {
            isNull = reader.ReadBoolean();
            if (!isNull)
            {
                string unparsed = Helper.ReadString(reader);
                Name = Helper.ReadString(reader);

                try
                {
                    Parsed = new ParsedString();
                    Parsed.TryParseString(unparsed);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(string.Format("#ERR! : can't parse the string, message: {0}", e.Message));
                    return false;
                }

                ObjType = Helper.TryResolveType(Parsed.objtype);

            }
            return true;
        }

        public bool Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
