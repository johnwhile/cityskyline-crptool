﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;


namespace CrpTool
{
    public class SimpleAES
    {
        private static byte[] key = new byte[32] { 13, 27, 119, 151, 224, 216, 8, 95, 14, 184, 217, 168, 137, 34, 32, 122, 21, 241, 158, 194, 137, 153, 96, 9, 24, 26, 47, 118, 231, 236, 59, 209 };
        private static byte[] vector = new byte[16] { 81, 101, 222, 3, 25, 8, 213, 21, 34, 55, 89, 144, 233, 32, 114, 56 };
        private ICryptoTransform encryptor;
        private ICryptoTransform decryptor;
        private UTF8Encoding encoder;

        public SimpleAES()
        {
            RijndaelManaged rijndaelManaged = new RijndaelManaged();
            encryptor = rijndaelManaged.CreateEncryptor(SimpleAES.key, SimpleAES.vector);
            decryptor = rijndaelManaged.CreateDecryptor(SimpleAES.key, SimpleAES.vector);
            encoder = new UTF8Encoding();
        }

        public string Encrypt(string unencrypted)
        {
            return Convert.ToBase64String(Encrypt(this.encoder.GetBytes(unencrypted)));
        }

        public string Decrypt(string encrypted)
        {
            return this.encoder.GetString(Decrypt(Convert.FromBase64String(encrypted)));
        }

        public byte[] Encrypt(byte[] buffer)
        {
            return Transform(buffer, this.encryptor);
        }

        public byte[] Decrypt(byte[] buffer)
        {
            return Transform(buffer, this.decryptor);
        }

        protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
        {
            MemoryStream memoryStream = new MemoryStream();
            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, transform, CryptoStreamMode.Write))
                cryptoStream.Write(buffer, 0, buffer.Length);
            return memoryStream.ToArray();
        }
    }
}
