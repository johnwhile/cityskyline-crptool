﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace CrpTool
{
    public partial class DebuggerForm : Form
    {
        public DebuggerTraceListener m_debugoutput;

        public DebuggerForm()
        {
            InitializeComponent();

            m_debugoutput = new DebuggerTraceListener(m_textbox);


        }

        /// <summary>
        /// Redirect Debug writing to a textblock
        /// </summary>
        public class DebuggerTraceListener : TraceListener
        {
            TextBoxBase m_textbox;

            public DebuggerTraceListener(TextBoxBase textbox)
            {
                m_textbox = textbox;
            }

            public override void Write(string message)
            {
               if (m_textbox!=null)
                {
                    m_textbox.Text += message;
                }
            }

            public override void WriteLine(string message)
            {
                if (m_textbox != null)
                {
                    m_textbox.Text += message;
                    m_textbox.Text += "\r\n";
                }
            }
        }
    }
}
