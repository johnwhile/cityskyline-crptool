﻿using System;
using System.Diagnostics;
using System.IO;

namespace CrpTool
{
    public class ParsedString
    {
        public string objtype;
        public string assembly;
        public Version version;
        public string culture;
        public string key;

        public string toparse;

        /// <summary>
        /// Generate exceptions
        /// </summary>
        public void TryParseString(string toparse)
        {
            if (string.IsNullOrEmpty(toparse)) throw new ArgumentNullException("String to parse is empty");

            this.toparse = toparse;

            string[] splitted;
            string[] substring = toparse.Split(new string[] { ", " }, StringSplitOptions.None);

            if (substring.Length != 5) throw new Exception("Error parsing string, requires five sector separate by comma");

            objtype = substring[0];
            assembly = substring[1];

            splitted = substring[2].Split(new string[] { "Version=" }, StringSplitOptions.RemoveEmptyEntries);
            version = Version.Parse(splitted[0]);

            splitted = substring[3].Split(new string[] { "Culture=" }, StringSplitOptions.RemoveEmptyEntries);
            culture = splitted[0];

            splitted = substring[4].Split(new string[] { "PublicKeyToken=" }, StringSplitOptions.RemoveEmptyEntries);
            key = splitted[0];

            //version = Version.Parse(substring[2].Remove(0, 10));//remove Version = 
            //culture = substring[3].Remove(0, 10); //remove Culture = 
            //key = substring[4].Remove(0, 17); //remove PublicKeyToken = 
        }
    }
}
