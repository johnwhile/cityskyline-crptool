﻿using System.Diagnostics;
using System.IO;
using System;
using System.Runtime.Serialization;

namespace CrpTool
{
    public abstract class AssetData : IMySerialized , IMyExportable
    {
        public ParsedObject header;

        protected PackageAsset package;


        public AssetData(PackageAsset package)
        {
            this.package = package;
            header = new ParsedObject();
        }

        /// <summary>
        /// Is possible a null asset ? NO !
        /// </summary>
        public bool IsNull
        {
            get { return header.isNull; }
        }

        public abstract AssetType AssetType { get; }

        public abstract bool Deserialize(BinaryReader reader);

        public abstract bool Serialize(BinaryWriter writer);

        /// <summary>
        /// file extension is given by method. For undeserialized data is set as *.unknown
        /// </summary>
        public abstract Result Export(string filename);

        public override string ToString()
        {
            return string.Format("Asset {0} : {1} isNull:{2}", AssetType, header.Name, IsNull);
        }


    }
}
