﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace CrpTool
{

    public struct ModInfo
    {
        public string modName;
        public ulong modWorkshopID;
    }

    /// <summary>
    /// CustomAssetMetaData from Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
    /// </summary>
    [Serializable]
    public class AssetCustomMetaData : AssetData
    {
        public enum CustomMetaDataType
        {
            Building = 0,
            Prop = 1,
            Tree = 2,
            Vehicle = 3,
            Trailer = 4,
            Unknown = 5,
            SubBuilding = 6,
            PropVariation = 7,
            Citizen = 8,
            Road = 9,
            RoadElevation = 10,
            Pillar = 11
        }

        public CustomMetaDataType type = CustomMetaDataType.Unknown;
        public int width = -1;
        public int length = -1;
        public Level level = Level.None;
        public string name;
        public DateTime timeStamp;
        public ObjectReference imageRef;
        public ObjectReference steamPreviewRef;
        public string[] steamTags;
        public string guid;
        public ModInfo[] mods;
        public Service service;
        public SubService subService;
        public VehicleType vehicleType;
        public SteamHelper.DLC_BitMask dlcMask; //mantein original reference of Assembly-CSharp.dll for new DLC
        public int triangles;
        public int lodTriangles;
        public int textureHeight;
        public int textureWidth;
        public int lodTextureHeight;
        public int lodTextureWidth;
        public string templateName;
        public ObjectReference userDataRef;

        public ObjectReference assetRef;

        /*
        public CustomAssetMetaData.Type type = CustomAssetMetaData.Type.Unknown;
        public int width = -1;
        public int length = -1;
        public ItemClass.Level level = ItemClass.Level.None;
        public string name;
        public DateTime timeStamp;
        public Package.Asset imageRef;
        public Package.Asset steamPreviewRef;
        public string[] steamTags;
        public string guid;
        public ModInfo[] mods;
        public ItemClass.Service service;
        public ItemClass.SubService subService;
        public VehicleInfo.VehicleType vehicleType;
        public SteamHelper.DLC_BitMask dlcMask;
        public int triangles;
        public int lodTriangles;
        public int textureHeight;
        public int textureWidth;
        public int lodTextureHeight;
        public int lodTextureWidth;
        public string templateName;
        public Package.Asset userDataRef;
        */

        public override AssetType AssetType
        {
            get { return AssetType.CustomAssetMetaData; }
        }


        public AssetCustomMetaData(PackageAsset package) : base(package)
        {

        }


        public override bool Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Try to match the properties found in the file structure and current class. Generate Exception
        /// </summary>
        public override bool Deserialize(BinaryReader reader)
        {
            if (!header.Deserialize(reader)) return false;

            if (header.isNull) return true;

            int prop_count = reader.ReadInt32();

            Type classtype = typeof(AssetCustomMetaData);

            for (int i = 0; i < prop_count; i++)
            {
                ParsedObject properties = new ParsedObject();
                if (!properties.Deserialize(reader)) return false;

                if (!properties.isNull)
                {
                    object value = null;
                    FieldInfo info = classtype.GetField(properties.Name);

                    Debug.Assert((info != null), string.Format("#ERR! : properties \"{0}\" not implemented", properties.Name));

                    string new_objtype = properties.Parsed.objtype.Replace('+', '.');

                    if (Helper.Parser.ContainsKey(new_objtype))
                    {
                        DeserializerDelegate deserializer = Helper.Parser[new_objtype];
                        value = deserializer(reader);
                    }
                    else throw new Exception(string.Format("not found deserializer method for \"{0}\"", new_objtype));

                    info.SetValue(this, value);
                }
            }

            package.file.AssetList.TryGetValue(imageRef.ToString(), out imageRef.reference);
            package.file.AssetList.TryGetValue(steamPreviewRef.ToString(), out steamPreviewRef.reference);
            package.file.AssetList.TryGetValue(userDataRef.ToString(), out userDataRef.reference);
            package.file.AssetList.TryGetValue(assetRef.ToString(), out assetRef.reference);

            /*
            catch (Exception e)
            {
            Debug.WriteLine("#ERR! : can't deserialize AssetCustomMetaData with error : " + e.Message);
            return false;
            }
            /*byte[] bin = new byte[lenght];
            reader.Read(bin, 0, (int)lenght);

            using (Stream stream = new MemoryStream(bin))
            {
            IFormatter formatter = new BinaryFormatter();
            CustomAssetMetaData data = new CustomAssetMetaData();
            data = (CustomAssetMetaData)formatter.Deserialize(reader.BaseStream);
            }*/

            return true;
        }

        public override Result Export(string fullfilename)
        {
            fullfilename = fullfilename + ".txt";
            //fullfilename = Path.ChangeExtension(fullfilename, "txt");

            Debug.WriteLine(string.Format("> saving editable text file \"{0}\"", fullfilename));

            using (StreamWriter file = File.CreateText(fullfilename))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                writer.Formatting = Formatting.Indented;

                writer.WriteComment("CustomAssetMetaData file, for value type read the source code");
                writer.QuoteName = false;
                writer.WriteWhitespace("\r\n\r\n");

                writer.WriteValue("CustomAssetMetaData");
                writer.WriteStartObject();

                writer.WritePropertyName("type");
                writer.WriteValue(type.ToString());

                writer.WritePropertyName("width");
                writer.WriteValue(width);

                writer.WritePropertyName("length");
                writer.WriteValue(length);

                writer.WritePropertyName("level");
                writer.WriteValue(level.ToString());

                writer.WritePropertyName("name");
                writer.WriteValue(name);

                writer.WritePropertyName("timeStamp");
                writer.WriteValue(timeStamp);

                writer.WritePropertyName("imageRef");
                if (imageRef.reference != null && !string.IsNullOrWhiteSpace(imageRef.reference.ExportedFilename))
                    writer.WriteValue(imageRef.reference.ExportedFilename);
                else writer.WriteValue(imageRef.checksum);

                writer.WritePropertyName("steamPreviewRef");
                if (steamPreviewRef.reference != null && !string.IsNullOrWhiteSpace(steamPreviewRef.reference.ExportedFilename))
                    writer.WriteValue(steamPreviewRef.reference.ExportedFilename);
                else writer.WriteValue(steamPreviewRef.checksum);


                writer.WritePropertyName("steamTags");
                writer.Formatting = Formatting.None;
                writer.WriteStartArray();
                foreach (string str in steamTags) { writer.WriteValue(str); }
                writer.WriteEnd();
                writer.Formatting = Formatting.Indented;

                writer.WritePropertyName("guid");
                writer.WriteValue(guid);

                writer.WritePropertyName("mods");
                writer.WriteStartArray();
                foreach (ModInfo info in mods)
                {
                    writer.WriteStartArray();
                    writer.Formatting = Formatting.None;
                    writer.WriteValue(info.modName);
                    writer.WriteValue(info.modWorkshopID);
                    writer.WriteEndArray();
                    writer.Formatting = Formatting.Indented;
                }
                writer.WriteEnd();

                writer.WritePropertyName("service");
                writer.WriteValue(service.ToString());

                writer.WritePropertyName("subService");
                writer.WriteValue(subService.ToString());

                writer.WritePropertyName("vehicleType");
                writer.WriteValue(vehicleType.ToString());

                writer.WritePropertyName("dlcMask");
                writer.WriteValue(dlcMask.ToString());

                writer.WritePropertyName("triangles");
                writer.WriteValue(triangles);

                writer.WritePropertyName("triangles");
                writer.WriteValue(triangles);

                writer.WritePropertyName("lodTriangles");
                writer.WriteValue(lodTriangles);

                writer.WritePropertyName("textureHeight");
                writer.WriteValue(textureHeight);

                writer.WritePropertyName("textureWidth");
                writer.WriteValue(textureWidth);

                writer.WritePropertyName("lodTextureHeight");
                writer.WriteValue(lodTextureHeight);

                writer.WritePropertyName("lodTextureWidth");
                writer.WriteValue(lodTextureWidth);

                writer.WritePropertyName("templateName");
                writer.WriteValue(templateName);

                writer.WritePropertyName("userDataRef");
                if (userDataRef.reference != null && !string.IsNullOrWhiteSpace(userDataRef.reference.ExportedFilename))
                    writer.WriteValue(userDataRef.reference.ExportedFilename);
                else writer.WriteValue(userDataRef.checksum);

                writer.WritePropertyName("assetRef");
                if (assetRef.reference != null && !string.IsNullOrWhiteSpace(assetRef.reference.ExportedFilename))
                    writer.WriteValue(assetRef.reference.ExportedFilename);
                else writer.WriteValue(assetRef.checksum);

                writer.WriteEndObject();
            }
            return Result.OK;
        }
    }
}
