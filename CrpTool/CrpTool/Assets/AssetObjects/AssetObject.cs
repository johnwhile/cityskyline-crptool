﻿using CrpTool.AssetObjects;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace CrpTool.AssetObjects
{

    public class AssetObject : AssetData
    {
        object obj;

        public override AssetType AssetType
        {
            get { return AssetType.Object; }
        }

        public AssetObject(PackageAsset package) : base(package)
        {

        }

        public override bool Deserialize(BinaryReader reader)
        {
            if (!header.Deserialize(reader)) return false;
            if (IsNull) return true;

            switch (header.Parsed.objtype)
            {
                case "BuildingInfoGen": obj = new BuildingInfoGenObject(); break;
                case "AssetDataWrapper+UserAssetData": obj = new UserAssetDataObject(); break;
                default: Debug.WriteLine(string.Format("#ERR! : can't deserialize the assetobject {0}", header.Parsed.objtype)); return false;
            }

            int fieldcount = reader.ReadInt32();

            for (int i = 0; i < fieldcount; i++)
            {
                ParsedObject properties = new ParsedObject();
                if (!properties.Deserialize(reader)) return false;

                if (!properties.isNull)
                {
                    object value = null;
                    FieldInfo info = obj.GetType().GetField(properties.Name);

                    Debug.Assert((info != null), string.Format("#ERR! : properties \"{0}\" not implemented", properties.Name));

                    string new_objtype = properties.Parsed.objtype.Replace('+', '.');

                    if (Helper.Parser.ContainsKey(new_objtype))
                    {
                        DeserializerDelegate deserializer = Helper.Parser[new_objtype];
                        value = deserializer(reader);
                    }
                    else throw new Exception(string.Format("not found deserializer method for \"{0}\"", new_objtype));

                    info.SetValue(obj, value);
                }
            }



            return true;
        }

        public override bool Serialize(BinaryWriter writer)
        {
            throw new System.NotImplementedException();
        }

        public override Result Export(string fullfilename)
        {
            if (obj != null && obj is IMyExportable) return ((IMyExportable)obj).Export(fullfilename);
            else return Result.OK;
        }
    }
}
