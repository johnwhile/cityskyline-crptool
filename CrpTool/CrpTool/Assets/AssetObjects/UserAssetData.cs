﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrpTool.AssetObjects
{
    public class UserAssetDataObject : IMyExportable
    {
        public Dictionary<string, byte[]> m_AssetData;

        public Result Export(string fullfilename)
        {
            fullfilename = fullfilename + ".txt";
            //fullfilename = Path.ChangeExtension(fullfilename, "txt");

            Debug.WriteLine(string.Format("> saving editable text file \"{0}\"", fullfilename));

            using (StreamWriter file = File.CreateText(fullfilename))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                writer.Formatting = Formatting.Indented;

                writer.WriteComment("UserAssetData file, for value type read the source code");
                writer.QuoteName = false;
                writer.WriteWhitespace("\r\n\r\n");

                writer.WriteStartObject();
                writer.WritePropertyName("m_AssetData");
                writer.WriteStartArray();

                foreach (KeyValuePair<string, byte[]> data in m_AssetData)
                {
                    writer.WritePropertyName(data.Key);
                    writer.WriteValue(data.Value);
                }
                writer.WriteEndArray();

                writer.WriteEndObject();
            }
            return Result.OK;
        }
    }
}
