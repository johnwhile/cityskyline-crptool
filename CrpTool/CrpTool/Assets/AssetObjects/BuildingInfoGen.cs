﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;


namespace CrpTool.AssetObjects
{

    public class BuildingInfoGenObject : IMyExportable
    {
        public Vector2[] m_baseArea;
        public Vector2[] m_baseNormals;
        public Vector3 m_size;
        public Vector3 m_min;
        public Vector3 m_max;
        public int m_walkWidth;
        public int m_walkLength;
        public float[] m_walkLevel;
        public float[] m_heights;
        public float m_triangleArea;
        public float m_uvmapArea;

        public BuildingInfoBase m_buildingInfo;

        public BuildingInfoGenObject()
        {

        }

        /*
        public bool Deserialize(BinaryReader reader)
        {
            int prop_count = reader.ReadInt32();
            Type classtype = typeof(BuildingInfoGenObject);

            for (int i = 0; i < prop_count; i++)
            {
                ParsedObject properties = new ParsedObject();
                if (!properties.Deserialize(reader)) return false;

                if (!properties.isNull)
                {
                    object value = null;
                    FieldInfo info = classtype.GetField(properties.Name);

                    Debug.Assert((info != null), string.Format("#ERR! : properties \"{0}\" not implemented", properties.Name));

                    string new_objtype = properties.Parsed.objtype.Replace('+', '.');

                    if (Helper.Parser.ContainsKey(new_objtype))
                    {
                        DeserializerDelegate deserializer = Helper.Parser[new_objtype];
                        value = deserializer(reader);
                    }
                    else throw new Exception(string.Format("not found deserializer method for \"{0}\"", new_objtype));


                    //if (info==null || info.FieldType.ToString() != properties.Parsed.objtype)
                    //throw new Exception(string.Format("the properties type \"{0}\" not match with current class member type \"{1}\"", info.FieldType.ToString(), properties.Parsed.objtype));

                    if (info != null) info.SetValue(this, value);
                }
            }
            return true;
        }

        public bool Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }
        */
        public Result Export(string fullfilename)
        {
            fullfilename = fullfilename + ".txt";
            //fullfilename = Path.ChangeExtension(fullfilename, "txt");

            Debug.WriteLine(string.Format("> saving editable text file \"{0}\"", fullfilename));

            using (StreamWriter file = File.CreateText(fullfilename))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                writer.Formatting = Formatting.Indented;

                writer.WriteComment("BuildingInfoGen file, for value type read the source code");
                writer.QuoteName = false;
                writer.WriteWhitespace("\r\n\r\n");

                writer.WriteStartObject();
                writer.WritePropertyName("m_baseArea");
                writer.WriteStartArray();
                foreach (Vector2 vect in m_baseArea)
                {
                    writer.WriteValue(vect.x);
                    writer.WriteValue(vect.y);
                }
                writer.WriteEndArray();

                writer.WritePropertyName("m_baseNormals");
                writer.WriteStartArray();
                foreach (Vector2 vect in m_baseNormals)
                {
                    writer.WriteValue(vect.x);
                    writer.WriteValue(vect.y);
                }
                writer.WriteEndArray();

                writer.WritePropertyName("m_size");
                writer.WriteStartArray();
                writer.WriteValue(m_size.x);
                writer.WriteValue(m_size.y);
                writer.WriteValue(m_size.z);
                writer.WriteEndArray();

                writer.WritePropertyName("m_min");
                writer.WriteStartArray();
                writer.WriteValue(m_min.x);
                writer.WriteValue(m_min.y);
                writer.WriteValue(m_min.z);
                writer.WriteEndArray();

                writer.WritePropertyName("m_max");
                writer.WriteStartArray();
                writer.WriteValue(m_max.x);
                writer.WriteValue(m_max.y);
                writer.WriteValue(m_max.z);
                writer.WriteEndArray();

                writer.WritePropertyName("m_walkWidth");
                writer.WriteValue(m_walkWidth);

                writer.WritePropertyName("m_walkLength");
                writer.WriteValue(m_walkLength);

                writer.WritePropertyName("m_walkLevel");
                writer.WriteStartArray();
                foreach (float f in m_walkLevel) { writer.WriteValue(f); }
                writer.WriteEnd();

                writer.WritePropertyName("m_heights");
                writer.WriteStartArray();
                foreach (float f in m_heights) { writer.WriteValue(f); }
                writer.WriteEndArray();

                writer.WritePropertyName("m_triangleArea");
                writer.WriteValue(m_triangleArea);

                writer.WritePropertyName("m_uvmapArea");
                writer.WriteValue(m_uvmapArea);

                writer.WriteEndObject();
            }
            return Result.OK;
        }
    }
}
