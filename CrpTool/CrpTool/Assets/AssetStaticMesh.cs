﻿using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

using FbxWrapper;
using FbxVector2 = FbxWrapper.Vector2;
using FbxVector3 = FbxWrapper.Vector3;
using FbxVector4 = FbxWrapper.Vector4;
using FbxColor = FbxWrapper.Colour;


namespace CrpTool
{
    public class AssetStaticMesh : AssetData
    {
        public List<Vector3> vertices;
        public List<Vector2> textcoord0;
        public List<Vector3> normals;
        public List<Vector4> tangents;
        public List<Vector4> colours;
        public List<BoneWeight> boneweight;
        public List<Matrix4x4> bindposes;
        public List<List<Triangle>> submeshes;

        public override AssetType AssetType
        {
            get { return AssetType.StaticMesh; }
        }

        public AssetStaticMesh(PackageAsset package) : base(package)
        {

        }

        public override bool Deserialize(BinaryReader reader)
        {
            if (!header.Deserialize(reader)) return false;
            if (header.isNull) return true;

            int count = reader.ReadInt32();
            vertices = new List<Vector3>(count);
            for (int i = 0; i < count; i++) { vertices.Add(Vector3.Deserializer(reader)); }

            count = reader.ReadInt32();
            colours = new List<Vector4>(count);
            for (int i = 0; i < count; i++) { colours.Add(Vector4.Deserializer(reader)); }

            count = reader.ReadInt32();
            textcoord0 = new List<Vector2>(count);
            for (int i = 0; i < count; i++) { textcoord0.Add(Vector2.Deserializer(reader)); }

            count = reader.ReadInt32();
            normals = new List<Vector3>(count);
            for (int i = 0; i < count; i++) { normals.Add(Vector3.Deserializer(reader)); }

            count = reader.ReadInt32();
            tangents = new List<Vector4>(count);
            for (int i = 0; i < count; i++) { tangents.Add(Vector4.Deserializer(reader)); }

            count = reader.ReadInt32();
            boneweight = new List<BoneWeight>(count);
            for (int i = 0; i < count; i++) { boneweight.Add(BoneWeight.Deserializer(reader)); }

            count = reader.ReadInt32();
            bindposes = new List<Matrix4x4>(count);
            for (int i = 0; i < count; i++) { bindposes.Add(Matrix4x4.Deserializer(reader)); }

            count = reader.ReadInt32();
            submeshes = new List<List<Triangle>>(count);
            for (int i = 0; i < count; i++)
            {
                int triangleCount = reader.ReadInt32();
                if (triangleCount % 3 != 0)
                {
                    Debug.WriteLine("#ERR! : Not a valid submesh");
                    return false;
                }
                triangleCount /= 3;

                List<Triangle> indices = new List<Triangle>(triangleCount);
                for (int j = 0; j < triangleCount; j++) { indices.Add(Triangle.Deserializer(reader)); }

                submeshes.Add(indices);
            }
            return true;
        }

        public override bool Serialize(BinaryWriter writer)
        {
            throw new System.NotImplementedException();
        }

        FbxVector2 ConvertV2(Vector2 v) { return new FbxVector2(v.x, v.y); }
        FbxVector3 ConvertV3(Vector3 v) { return new FbxVector3(v.x, v.y, v.z); }
        FbxVector4 ConvertV4(Vector4 v) { return new FbxVector4(v.x, v.y, v.z, v.w); }
        FbxVector4 ConvertV4(Vector3 v) { return new FbxVector4(v.x, v.y, v.z, 1); }
        FbxColor ConvertC(Vector4 v) { return new FbxColor(v.x, v.y, v.z, v.w); }

        public override Result Export(string fullfilename)
        {
            Debug.WriteLine(string.Format("> esperimental saving fbx file \"{0}\"", fullfilename));

            Manager manager = new Manager();

            Scene scene = new Scene("Crp");
            Node root = scene.RootNode;
            Node node = new Node(AttributeType.Mesh, base.header.Name);

            Mesh staticmesh = node.Mesh;

            if (staticmesh != null)
            {

                staticmesh.ControlPointsCount = vertices.Count;

                FbxVector3[] fbxvertices = new FbxVector3[vertices.Count];
                for (int i = 0; i < vertices.Count; i++) fbxvertices[i] = ConvertV3(vertices[i]);
                staticmesh.ControlPoints = fbxvertices;

                FbxVector2[] fbxuvs = new FbxVector2[textcoord0.Count];
                for (int i = 0; i < textcoord0.Count; i++) fbxuvs[i] = ConvertV2(textcoord0[i]);
                staticmesh.SetTextCoords(fbxuvs, MappingMode.ByControlPoint, ReferenceMode.Direct);

                FbxVector4[] fbxtangents = new FbxVector4[tangents.Count];
                for (int i = 0; i < tangents.Count; i++) fbxtangents[i] = ConvertV4(tangents[i]);
                staticmesh.SetTangents(fbxtangents, MappingMode.ByControlPoint, ReferenceMode.Direct);

                FbxVector3[] fbxnormals = new FbxVector3[normals.Count];
                for (int i = 0; i < normals.Count; i++) fbxnormals[i] = ConvertV3(normals[i]);
                staticmesh.SetNormals(fbxnormals, MappingMode.ByControlPoint, ReferenceMode.Direct);

                FbxColor[] fbxcolors = new FbxColor[colours.Count];
                for (int i = 0; i < colours.Count; i++) fbxcolors[i] = ConvertC(colours[i]);
                staticmesh.SetVertexColours(fbxcolors, MappingMode.ByControlPoint, ReferenceMode.Direct);

                foreach (List<Triangle> meshgroup in submeshes)
                {
                    foreach (Triangle tri in meshgroup)
                    {
                        staticmesh.AddPolygon(tri.index);
                    }
                }

                root.AddChild(node);
                Scene.Export(scene, fullfilename + ".fbx", -1);
            }
            else
            {
                Debug.WriteLine("ERROR IN FBX WRAPPER");
            }

            fullfilename = fullfilename + ".txt";
            //fullfilename = Path.ChangeExtension(fullfilename, "txt");

            Debug.WriteLine(string.Format("> saving obj file \"{0}\"", fullfilename));

            using (StreamWriter file = File.CreateText(fullfilename))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                writer.Formatting = Formatting.Indented;

                writer.WriteComment("UnityEngine StatiMesh file, need to convert this file in FBX format");
                writer.QuoteName = false;
                writer.WriteWhitespace("\r\n\r\n");

                writer.WriteStartObject();

                writer.WritePropertyName("vertices");
                writer.WriteStartArray();
                foreach (Vector3 vect in vertices)
                {
                    writer.WriteStartArray();
                    writer.Formatting = Formatting.None;
                    writer.WriteValue(vect.x);
                    writer.WriteValue(vect.y);
                    writer.WriteValue(vect.z);
                    writer.WriteEndArray();
                    writer.Formatting = Formatting.Indented;

                }
                writer.WriteEndArray();
                writer.WriteEndObject();
            }
            return Result.OK;
        }
    }
}
