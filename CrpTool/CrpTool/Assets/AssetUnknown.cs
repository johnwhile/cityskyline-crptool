﻿using System.Diagnostics;
using System.IO;

namespace CrpTool
{
    /// <summary>
    /// Not implemented asset used when occur an undefined asset type
    /// </summary>
    public class AssetUnknown : AssetData
    {
        protected int size;
        protected byte[] data;

        public override AssetType AssetType
        {
            get { return AssetType.Unknow; }
        }

        /// <summary>
        /// </summary>
        /// <param name="size">require also size of data because can't be extrapolated</param>
        public AssetUnknown(PackageAsset package, int size) :
            base(package)
        {
            this.size = size;
        }

        public override bool Serialize(BinaryWriter writer)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// the data are not deserialized but stored
        /// </summary>
        public override bool Deserialize(BinaryReader reader)
        {
            header.isNull = reader.ReadBoolean();
            if (!IsNull)
            {
                try { data = reader.ReadBytes(size - 1); } //-1 because first byte always check if asset is null
                catch { return false; }
            }

            return true;
        }

        public override Result Export(string fullfilename)
        {
            fullfilename = fullfilename + ".unknown";
            //fullfilename = Path.ChangeExtension(fullfilename, "unknown");

            Debug.WriteLine(string.Format("> saving unknown data as \"{0}\"", fullfilename));

            using (Stream stream = File.Open(fullfilename, FileMode.Create))
            {
                stream.Write(data, 0, data.Length);
                stream.Flush();
                stream.Close();
            }

            return Result.OK;
        }
    }


}
