﻿using System.Diagnostics;
using System.IO;


namespace CrpTool
{
    public class AssetTexture : AssetData
    {
        public enum TextureType
        {
            Unknown,
            PNG,
            DDS
        }

        public bool forceLinear = false;
        public int anisoLevel = 1;
        public int formattedSize;
        
        public TextureType fileExtension;

        protected byte[] data;

        public override AssetType AssetType
        {
            get { return AssetType.Texture; }
        }

        public AssetTexture(PackageAsset package) :
            base(package)
        {

        }

        public override bool Deserialize(BinaryReader reader)
        {
            if (!header.Deserialize(reader)) return false;

            if (!IsNull)
            {
                forceLinear = reader.ReadBoolean();
                if (package.file.header.FormatVersion >= 6) anisoLevel = reader.ReadInt32();
                formattedSize = reader.ReadInt32();
                data = reader.ReadBytes(formattedSize);
                fileExtension = getImageFormat();
            }
            return true;
        }

        public override bool Serialize(BinaryWriter writer)
        {
            throw new System.NotImplementedException();
        }

        TextureType getImageFormat()
        {
            if (data[0] == 'D' && data[1] == 'D' && data[2] == 'S') return TextureType.DDS;

            if (data[0] == 137 &&
                data[1] == 'P' &&
                data[2] == 'N' &&
                data[3] == 'G' &&
                data[4] == 13 &&
                data[5] == 10 &&
                data[6] == 26 &&
                data[7] == 10)
                return TextureType.PNG;

            return TextureType.Unknown;
        }

        public override Result Export(string fullfilename)
        {
            Debug.WriteLineIf(fileExtension == TextureType.Unknown, "#WARNING! : image have unknown extension");

            fullfilename = fullfilename + '.' + fileExtension;
            //fullfilename = Path.ChangeExtension(fullfilename, fileExtension.ToString());

            Debug.WriteLine(string.Format("> saving texture file \"{0}\"", fullfilename));

            using (Stream stream = File.Open(fullfilename, FileMode.Create))
            {
                stream.Write(data, 0, data.Length);
                stream.Flush();
                stream.Close();
            }

            return Result.OK;
        }


    }
}
