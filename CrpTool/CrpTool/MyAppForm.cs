﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace CrpTool
{
    public partial class MyAppForm : Form
    {
        public DebuggerForm debugform;

        public MyAppForm()
        {
            InitializeComponent();
            debugform = new DebuggerForm();
            debugform.Show(this);
            debugform.Location = new Point(this.Location.X + this.Size.Width + 10, this.Location.Y);
            
            Debug.Listeners.Add(debugform.m_debugoutput);
            Debug.WriteLine("Log...");
        }

        private void btnUnpack_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = openFileDialog.ShowDialog();
            PackageFile package;

            if (result == DialogResult.OK)
            {
                Debug.WriteLine("Open file : " + openFileDialog.FileName);

                package = new PackageFile();

                if (package.OpenFile(openFileDialog.FileName) == Result.OK)
                {
                    if (package.ExportToFiles() != Result.OK)
                    {
                        Debug.WriteLine("Error when creating files");
                    }
                }
                else
                {
                    Debug.WriteLine("Error in deserialization");
                }
            }
            Debug.WriteLine(result);
        }

        private void btnPack_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Work in progress");
        }
    }
}
