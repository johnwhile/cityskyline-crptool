﻿using System.Diagnostics;
using System.IO;


namespace CrpTool
{
    public abstract class Field : IMySerialized
    {
        protected ParsedObject header;

        public Field(PackageFile package)
        {
            header = new ParsedObject();
        }
        /// <summary>
        /// Is possible a null asset ? NO !
        /// </summary>
        public bool IsNull
        {
            get { return header.isNull; }
        }

        public abstract bool Deserialize(BinaryReader reader);

        public abstract bool Serialize(BinaryWriter writer);
    }
}
