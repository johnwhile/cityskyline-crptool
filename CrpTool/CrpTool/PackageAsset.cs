﻿using System;
using System.Diagnostics;
using System.IO;

using CrpTool.AssetObjects;

namespace CrpTool
{
    /// <summary>
    /// </summary>
    public class PackageAsset : IMySerialized
    {
        public string ExportedFilename;

        internal PackageFile file;

        public int index;
        public AssetData data;

        /// <summary>
        /// composed by asset's name + checksum string
        /// </summary>
        protected string uniquename;
        protected string checksum;
        protected AssetType type;
        protected long offset;
        protected long size;

        public PackageAsset(PackageFile file, string uniquename, string checksum, AssetType type, long offset, long size, int index = -1)
        {
            this.file = file;
            this.index = index;
            this.uniquename = uniquename;
            this.checksum = checksum;
            this.type = type;
            this.offset = offset;
            this.size = size;
        }

        /// <summary>
        /// The semplified asset name
        /// </summary>
        public string AssetName
        {
            get
            {
                if (data != null && !data.IsNull && data.header != null && !string.IsNullOrWhiteSpace(data.header.Name))
                    return data.header.Name;
                return uniquename;
            }
        }
        /// <summary>
        /// The full asset name
        /// </summary>
        public string UniqueName
        {
            get
            {
                if (data != null && !data.IsNull && data.header != null && !string.IsNullOrWhiteSpace(data.header.Name))
                    return string.Format("{0} - {1}", data.header.Name, checksum);
                return uniquename;
            }
        }

        /// <summary>
        /// Used also as object's reference
        /// </summary>
        public string Checksum
        {
            get { return checksum; }
        }

        public bool IsMainAsset
        {
            get { return file.header.MainAsset == AssetName; }
        }

        /// <summary>
        /// Create the binary file
        /// </summary>
        public Result ExportData(string rootFolder)
        {
            ExportedFilename = AssetName;

            if (string.IsNullOrWhiteSpace(ExportedFilename)) ExportedFilename = checksum;
            if (string.IsNullOrWhiteSpace(ExportedFilename)) ExportedFilename = this.GetHashCode().ToString();

            ExportedFilename = rootFolder + '\\' + ExportedFilename;
            //string fullfilename = Path.Combine(rootFolder, filename);

            return data.Export(ExportedFilename);
        }

        public bool Deserialize(BinaryReader reader)
        {
            data = null;

            Debug.WriteLine(string.Format("> read asset {0} {1}", index, type));

            // go to asset data
            long position = reader.BaseStream.Position;
            if (position + size > reader.BaseStream.Length)
            {
                Debug.WriteLine("#ERR! : Asset can't be read because wrong file position");
                return false;
            }

            reader.BaseStream.Seek(offset + file.header.HeaderOffset, SeekOrigin.Begin);

            switch (type)
            {
                case AssetType.Package: throw new Exception("Another package file contents in this package is not implemented");
                case AssetType.Texture: data = new AssetTexture(this); break;
                case AssetType.Object: data = new AssetObject(this); break;
                case AssetType.CustomAssetMetaData: data = new AssetCustomMetaData(this); break;
                case AssetType.StaticMesh: data = new AssetStaticMesh(this); break;

                default:
                    Debug.Indent();
                    Debug.WriteLine(string.Format("#ERR! : Unknown Asset type: {0}, data can't be deserialize", type));
                    Debug.Unindent();
                    data = new AssetUnknown(this, (int)size);
                    break;
            }

            // Asset type is implemented but an error occur when deserializing 
            if (!(data is AssetUnknown)  && !data.Deserialize(reader))
            {
                Debug.WriteLine("#WARNING! : can't be deserialize asset, try to export anyway binary data, file will be create with unknown format");
                reader.BaseStream.Seek(offset + file.header.HeaderOffset, SeekOrigin.Begin);
                data = new AssetUnknown(this, (int)size);
            }

            // Asset type is not implemented but can't be read binary data, something wrong in file format
            if (data is AssetUnknown && !data.Deserialize(reader))
            {
                Debug.WriteLine("#ERR! : can't read binary data, exit for safety");
                data = null;
                return false;
            }

            Debug.WriteLineIf(data.IsNull, "#WARNING! : asset is marked as null, why?");

            reader.BaseStream.Seek(position, SeekOrigin.Begin);

            return true;
        }

        public bool Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return string.Format("Asset_{0}", type);
        }
    }
}
