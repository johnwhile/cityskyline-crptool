﻿using System;
using System.Diagnostics;
using System.IO;


namespace CrpTool
{
    /// <summary>
    /// header of file
    /// </summary>
    public class PackageHeader : IMySerialized
    {
        public string ValidateID = "CRAP";
        public ushort FormatVersion = 6;
        public string PackageName;
        public string PackageAuthor;
        public uint PackageVersion;
        public string MainAsset;
        public long HeaderOffset;
        public int AssetCount;


        public bool Deserialize(BinaryReader reader)
        {
            reader.BaseStream.Seek(0, SeekOrigin.Begin);

            ValidateID = new string(reader.ReadChars(4));

            Debug.Assert(ValidateID == "CRAP", "Package is not recognized");

            FormatVersion = reader.ReadUInt16();

            PackageName = Helper.ReadString(reader);
            if (string.IsNullOrEmpty(PackageName)) PackageName = "Anonymous Package";

            string encrypted = Helper.ReadString(reader);
            if (!string.IsNullOrEmpty(encrypted) && encrypted != "") PackageAuthor = new SimpleAES().Decrypt(encrypted);
            else PackageAuthor = encrypted;

            PackageVersion = reader.ReadUInt32();

            MainAsset = Helper.ReadString(reader);

            Debug.WriteLine("PackageName = " + MainAsset);

            AssetCount = reader.ReadInt32();
            HeaderOffset = (long)reader.ReadUInt64();

            return true;
        }

        public bool Serialize(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
